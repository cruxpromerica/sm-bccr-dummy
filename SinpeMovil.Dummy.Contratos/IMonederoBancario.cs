﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using SinpeMovil.Dummy.Modelos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Contratos
{
	[ExceptionShielding()]
	[ServiceContract(Namespace = "http://bccr.fi.cr/servicios/pmb")]
	public interface IMonederoBancario
	{
		#region "Administrativo"

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		bool ServicioDisponible();

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		Respuesta SolicitarMonedero(Suscripcion solicitud);
		
		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		Respuesta InactivarMonedero(Suscripcion solicitud);

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		void PermitirRecibirTransacciones(bool permitir);

		#endregion

		#region "Envíos y consultas"

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		RespuestaPago EnviarPagoDesdeTelefono(Operacion pago);

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		RespuestaPago EnviarPagoDesdeCuenta(Operacion pago);

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		RespuestaPago ObtenerEstado(string referencia);

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		Suscripcion ObtenerInformacionTelefono(int numTelefono);

		[OperationContract]
		[FaultContract(typeof(PmbFault), Action = PmbFault.Pmb_Faults_Action)]
		List<DetalleOperacion> ObtenerDetalleOperaciones(System.DateTime fecLiquidacion);

		#endregion


	}
}