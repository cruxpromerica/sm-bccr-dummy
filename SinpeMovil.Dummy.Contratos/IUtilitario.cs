﻿using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using SinpeMovil.LogicaNegocio.Modelos;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;

namespace SinpeMovil.Dummy.Contratos
{
	[ServiceContract]
	public interface IUtilitario
	{
		[OperationContract]
		void BorrarPadronesDummy(int numTelefono);

		[OperationContract]
		void BorrarTransaccionesDummy(string codReferencia);

		[OperationContract]
		void RegistrarTransacciones(Transaccion transaccion, TransaccionHistorica transaccionHST);
	}
}
