﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.AccesoDatos.Contratos
{
	public interface IClientes : IContextoDatosBase
	{
		SinpeMovil.Dummy.Modelos.Cliente ObtenerCliente(string identificacion);

		int AgregarCliente(SinpeMovil.Dummy.Modelos.Cliente cliente);
	}
}
