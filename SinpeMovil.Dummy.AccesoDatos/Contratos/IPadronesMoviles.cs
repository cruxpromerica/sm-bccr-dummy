﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.AccesoDatos.Contratos
{
    public interface IPadronesMoviles : IContextoDatosBase
    {
       int AgregarPadron(SinpeMovil.Dummy.Modelos.Padron padron);

	   int ActualizarPadron(SinpeMovil.Dummy.Modelos.Padron padron);

	   Dummy.Modelos.Padron ObtenerPadronPorNumTelefono(int numTelefono);

    }
}
