﻿using SinpeMovil.Dummy.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.AccesoDatos.Contratos
{
    public interface ITransacciones : IContextoDatosBase
    {
        int Registrar(Transaccion transaccion);

		int Actualizar(Transaccion transaccion);

        Transaccion ObtenerTransaccion(DateTime fechaLiquidacion);

		Transaccion ObtenerEstado(string codReferencia);

		IList<Transaccion> ListarTransacciones(DateTime fechaLiquidacion);
    }
}
