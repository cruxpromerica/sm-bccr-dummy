﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Repositorios;
using SinpeMovil.AccesoDatos.Contratos;
using SinpeMovil.Dummy.Modelos;
using Repositorios.Mappings;

namespace Repositorios
{
    public sealed class Transacciones : ContextoDatos, ITransacciones
    {
        static Transacciones()
        {
            Database.SetInitializer<Transacciones>(null);
        }

        public DbSet<Transaccion> Transaccion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new SMTransaccionMap());
        }

        public int Registrar(Transaccion transaccion)
        {
            this.Agregar<Transaccion>(transaccion);
            return transaccion.IdTransaccion;
        }

        public int Actualizar(Transaccion transaccion)
        {
            this.Agregar<Transaccion>(transaccion);
            return transaccion.IdTransaccion;
        }

		public Transaccion ObtenerEstado(string codReferencia)
        {
            var transaccion = this.Obtener<Transaccion>(tran => tran.CodReferencia == codReferencia);
            return transaccion;
        }

		public Transaccion ObtenerTransaccion(DateTime fechaLiquidacion)
		{
			return this.Obtener<Transaccion>(tran => tran.FecValor == fechaLiquidacion);
		}

		public IList<Transaccion> ListarTransacciones(DateTime fechaLiquidacion)
		{
			return this.Listar<Transaccion>(tran => tran.FecValor == fechaLiquidacion);
		}
	}
}
