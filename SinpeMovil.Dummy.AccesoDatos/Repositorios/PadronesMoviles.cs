﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Repositorios;
using SinpeMovil.AccesoDatos.Contratos;

namespace Repositorios
{
	[UnityAutoRegistration()]
	public sealed class PadronesMoviles : ContextoDatos, IPadronesMoviles
	{
		static PadronesMoviles()
		{
			Database.SetInitializer<PadronesMoviles>(null);
		}

		public DbSet<SinpeMovil.Dummy.Modelos.Padron> Padron { get; set; }
		public DbSet<SinpeMovil.Dummy.Modelos.Cliente> Cliente { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Configurations.Add(new Repositorios.Mappings.SMPadronMap());
			modelBuilder.Configurations.Add(new Repositorios.Mappings.SMClienteMap());
		}

		int IPadronesMoviles.AgregarPadron(SinpeMovil.Dummy.Modelos.Padron padron)
		{
			this.Agregar<SinpeMovil.Dummy.Modelos.Padron>(padron);
			return padron.IdPadron;
		}
		private IList<SinpeMovil.Dummy.Modelos.Padron> ListarPadrones()
		{
			return this.Listar<SinpeMovil.Dummy.Modelos.Padron>(new string[] { "Cliente" });
		}
		int IPadronesMoviles.ActualizarPadron(SinpeMovil.Dummy.Modelos.Padron padron)
		{
			this.Actualizar<SinpeMovil.Dummy.Modelos.Padron>(padron);
			return padron.IdPadron;
		}

		public SinpeMovil.Dummy.Modelos.Padron ObtenerPadronPorNumTelefono(int numTelefono)
		{
			return this.Obtener<SinpeMovil.Dummy.Modelos.Padron>(padronMovil => padronMovil.NumTelefono == numTelefono, new string[] { "Cliente" });
		}
	}
}