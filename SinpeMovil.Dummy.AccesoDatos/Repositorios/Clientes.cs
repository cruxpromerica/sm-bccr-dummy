﻿using SinpeMovil.AccesoDatos.Contratos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorios
{
	[UnityAutoRegistration()]
	public sealed class Clientes : ContextoDatos, IClientes
	{
		static Clientes()
		{
			Database.SetInitializer<Clientes>(null);
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Configurations.Add(new Repositorios.Mappings.SMClienteMap());
		}

		public DbSet<SinpeMovil.Dummy.Modelos.Cliente> Cliente { get; set; }

		SinpeMovil.Dummy.Modelos.Cliente IClientes.ObtenerCliente(string identificacion)
		{
			SinpeMovil.Dummy.Modelos.Cliente cliente = default(SinpeMovil.Dummy.Modelos.Cliente);
			cliente = this.Obtener<SinpeMovil.Dummy.Modelos.Cliente>(x => x.Identificacion == identificacion);
			return cliente;
		}

		int IClientes.AgregarCliente(SinpeMovil.Dummy.Modelos.Cliente cliente)
		{
			this.Agregar<SinpeMovil.Dummy.Modelos.Cliente>(cliente);
			return cliente.IdCliente;
		}
	}
}