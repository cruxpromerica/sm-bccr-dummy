﻿using System.Data.Entity.ModelConfiguration;
using SinpeMovil.Dummy.Modelos;

namespace Repositorios.Mappings
{
    public class SMTransaccionMap : EntityTypeConfiguration<Transaccion>
    {
        public SMTransaccionMap() 
		{
            // Primary Key
            this.HasKey(t => t.IdTransaccion);

            // Properties
            this.Property(t => t.IdTransaccion).IsRequired();
            this.Property(t => t.CodReferencia).IsRequired().HasMaxLength(Constantes.TamanoMaximoNumeroCodRef);
            this.Property(t => t.PadronId).IsRequired();
            this.Property(t => t.CodEntidadOrigen).IsRequired();
            this.Property(t => t.CodEntidadDestino).IsRequired();
            this.Property(t => t.NumTelefono).IsRequired();
            this.Property(t => t.IdCliente).IsRequired();
            this.Property(t => t.NombreCliente).IsRequired().HasMaxLength(Constantes.TamanoMaximoNombreCliente);
            this.Property(t => t.CodMoneda).IsRequired();
            this.Property(t => t.Monto).IsRequired();
            this.Property(t => t.DesTransaccion).IsRequired().HasMaxLength(Constantes.TamanoMaximoDescTransaccion);
            this.Property(t => t.TipoTransaccion).IsRequired();
            this.Property(t => t.FecValor).IsRequired();
            this.Property(t => t.FecRegistro).IsRequired();
			this.Property(t => t.CodEstado).IsRequired();
			this.Property(t => t.CodRechazo).IsRequired();
			this.Property(t => t.Descripcion).IsRequired();
			this.Property(t => t.FecActualizacion).IsRequired();

            // Table & Column Mappings
            this.ToTable("SM_TRANSACCION_DUMMY");
            this.Property(t => t.IdTransaccion).HasColumnName("ID_TRANSACCION");
            this.Property(t => t.CodReferencia).HasColumnName("COD_REFERENCIA");
            this.Property(t => t.PadronId).HasColumnName("PADRON_ID");
            this.Property(t => t.CodEntidadOrigen).HasColumnName("COD_ENTIDAD_ORIGEN");
            this.Property(t => t.NumTelefono).HasColumnName("NUM_TELEFONO");
            this.Property(t => t.IdCliente).HasColumnName("CEDULA_CLIENTE");

            this.Property(t => t.NombreCliente).HasColumnName("NOMBRE_CLIENTE");
            this.Property(t => t.CodEntidadDestino).HasColumnName("COD_ENTIDAD_DESTINO");
            this.Property(t => t.CodMoneda).HasColumnName("MONEDA_ID");
            this.Property(t => t.Monto).HasColumnName("MONTO");

            this.Property(t => t.DesTransaccion).HasColumnName("DES_TRANSACCION");
            this.Property(t => t.TipoTransaccion).HasColumnName("TIPO_TRANSACCION");
            this.Property(t => t.FecValor).HasColumnName("FEC_VALOR");
            this.Property(t => t.FecRegistro).HasColumnName("FEC_REGISTRO");

			this.Property(t => t.CodEstado).HasColumnName("COD_ESTADO");
			this.Property(t => t.CodRechazo).HasColumnName("COD_RECHAZO");
			this.Property(t => t.Descripcion).HasColumnName("DESCRIPCION");
			this.Property(t => t.FecActualizacion).HasColumnName("FEC_ACTUALIZACION");
        }
    }
}
