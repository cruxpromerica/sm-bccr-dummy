﻿using System.Data.Entity.ModelConfiguration;
using SinpeMovil.Dummy.Modelos;

namespace Repositorios.Mappings
{
	public class SMClienteMap : EntityTypeConfiguration<Cliente>
	{
		public SMClienteMap()
		{
			// Primary Key
			this.HasKey(t => t.IdCliente);

			// Properties
			this.Property(t => t.Identificacion).IsRequired().HasMaxLength(Constantes.TamanoMaximoIdentificacionCliente);
			this.Property(t => t.NombreCliente).IsRequired().HasMaxLength(Constantes.TamanoMaximoNombreCliente);

			// Table & Column Mappings
			this.ToTable("SM_CLIENTE_DUMMY");
			this.Property(t => t.IdCliente).HasColumnName("ID_CLIENTE");
			this.Property(t => t.TipoId).HasColumnName("COD_TIPO_ID");
			this.Property(t => t.Identificacion).HasColumnName("IDENTIFICACION");
			this.Property(t => t.NombreCliente).HasColumnName("NOMBRE");
			this.Property(t => t.CodEstado).HasColumnName("COD_ESTADO");
		}
	}
}
