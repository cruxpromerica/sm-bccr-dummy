﻿using System.Data.Entity.ModelConfiguration;
using SinpeMovil.Dummy.Modelos;

namespace Repositorios.Mappings
{
    public class SMPadronMap : EntityTypeConfiguration<Padron>
    {
        public SMPadronMap() 
		{
            // Primary Key
            this.HasKey(t => t.IdPadron);

            // Properties
            this.Property(t => t.ClienteId).IsRequired();
            this.Property(t => t.NumTelefono).IsRequired();
            this.Property(t => t.CodEstado).IsRequired();

            // Table & Column Mappings
            this.ToTable("SM_PADRON_DUMMY");
            this.Property(t => t.IdPadron).HasColumnName("ID_PADRON");
            this.Property(t => t.ClienteId).HasColumnName("CLIENTE_ID");
            this.Property(t => t.NumTelefono).HasColumnName("NUM_TELEFONO");
            this.Property(t => t.CodEstado).HasColumnName("COD_ESTADO");
            this.Property(t => t.CodEntidad).HasColumnName("COD_ENTIDAD");


			// Relationships
			this.HasRequired(t => t.Cliente).WithMany().HasForeignKey(d => d.ClienteId);
        }
    }
}
