﻿using SinpeMovil.Dummy.Utilitarios.Ayudantes;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.AccesoDatos
{
    public class PadronesMoviles
    {
        public Respuesta SolicitarMonedero(Suscripcion suscripcion)
        {
			var ayudante = new AyudanteSuscripcion();
			return ayudante.ObtenerRespuesta(suscripcion);
        }

        public Respuesta InactivarMonedero(Suscripcion suscripcion)
        {
            var ayudante = new AyudanteInactivar();
            return ayudante.InactivarTelefono(suscripcion);
        }

        public Suscripcion ObtenerInformacionTelefono(int numTelefono)
        {
            var ayudante = new AyudanteObtenerInformacionTelefono();
            return ayudante.ObtenerInformacionTelefono(numTelefono);
        }

        public void PermitirRecibirTransacciones(bool permitir)
        {
			
        }

        public RespuestaPago EnviarPagoDesdeCuenta(Operacion pago)
        {
            var ayudante = new AyudantePagoDesdeCuenta();
            return ayudante.EnviarPagoDesdeCuenta(pago);
        }

        public RespuestaPago ObtenerEstado(string referencia)
        {
            return new RespuestaPago();
        }

        public List<DetalleOperacion> ObtenerDetalleOperaciones(DateTime fecLiquidacion)
        {

			var ayudante = new AyudanteObtenerDetalleOperaciones();
			return ayudante.ObtenerDetalleDeOperaciones(fecLiquidacion);
            
        }
    }
}
