﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
	public class AyudanteObtenerDetalleOperaciones : AyudanteBase<EscenarioObtenerDetalleOperaciones>
	{

		public AyudanteObtenerDetalleOperaciones()
		{

			_listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioObtenerDetalleOperaciones>(Recursos.Recursos.Prueba_ObtenerDetalleOperaciones_Dummy);

		}

		public override List<EscenarioObtenerDetalleOperaciones> ListaEscenarios
		{

			get { return _listaEscenarios; }

		}

		public List<DetalleOperacion> ObtenerDetalleDeOperaciones(DateTime fecLiquidacion)
		{

			List<DetalleOperacion> resultado = null;
            List<EscenarioObtenerDetalleOperaciones> escenario = ListaEscenarios.Where(e => e.FecLiquidacion == fecLiquidacion).ToList();

            foreach (var item in escenario)
			{
                DetalleOperacion detalle = new DetalleOperacion { CodEntidadDestino = item.CodEntidadDestino, CodEntidadOrigen = item.CodEntidadOrigen, CodMoneda = item.CodMoneda, CodReferencia = item.CodReferencia, FecValor = item.FecValor, Monto = item.Monto };
                resultado.Add(detalle);            
            }   
         
            return resultado;
		}
	}

}
