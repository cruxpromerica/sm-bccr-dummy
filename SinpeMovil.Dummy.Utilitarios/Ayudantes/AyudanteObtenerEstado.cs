﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos.Contenedores;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
    public class AyudanteObtenerEstado : AyudanteBase<EscenarioObtenerEstado>
    {
        public override List<EscenarioObtenerEstado> ListaEscenarios
        {
            get
            {
                return _listaEscenarios;
            }
        }

        public AyudanteObtenerEstado()
		{
            _listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioObtenerEstado>(Recursos.Recursos.ObtenerEstado_Dummy);
		}

        public RespuestaPago ObtenerInformacionTelefono(string referencia)
        {
            RespuestaPago resultado = null;
            EscenarioObtenerEstado escenario = ListaEscenarios.FirstOrDefault(e => e.CodReferencia == referencia);

            if (escenario.CodEstado == EstadoOperacion.Autorizada)
            {
                resultado = new RespuestaPago { CodEstado = escenario.CodEstado, Descripcion = escenario.Descripcion, FecValor = escenario.FecValor, NombreClienteDestino = escenario.NombreClienteDestino };
            }
            else if (escenario.CodEstado == EstadoOperacion.Rechazada)
            {
                resultado = new RespuestaPago { CodEstado = escenario.CodEstado, CodMotivoRechazo = escenario.CodMotivoRechazo, Descripcion = escenario.Descripcion, FecValor = escenario.FecValor, NombreClienteDestino = escenario.NombreClienteDestino };
            } 
            
            return resultado;
        }
    }
}