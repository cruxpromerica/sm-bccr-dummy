﻿using SinpeMovil.Dummy.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Utilitarios
{
	[System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
	public abstract class AyudanteBase<T> where T : EscenarioBase
	{
		protected List<T> _listaEscenarios;

		public abstract List<T> ListaEscenarios
		{
			get;
		}

	}
}
