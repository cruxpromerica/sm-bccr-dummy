﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
	public class AyudanteInactivar : AyudanteBase<EscenarioInactivar>
	{

		public AyudanteInactivar()
		{
			_listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioInactivar>(Recursos.Recursos.Prueba_Inactivar_Dummy);
		}


		public override List<EscenarioInactivar> ListaEscenarios
		{
			get
			{
				return _listaEscenarios;
			}
		}


		public Respuesta InactivarTelefono(Suscripcion suscripcion)
		{

			Respuesta resultado = null;
			EscenarioInactivar escenario = ListaEscenarios.FirstOrDefault(e => e.NumTelefono == suscripcion.NumTelefono &&
																		  e.Identificador == suscripcion.Identificacion &&
																		  e.NombreCliente == suscripcion.NombreCliente &&
																		  e.CodEntidad == suscripcion.CodEntidad);
			if (escenario.Resultado == "Exitoso")
			{

				resultado = new Respuesta { CodEstado = (EstadoOperacion)escenario.CodEstado, Descripcion = escenario.Descripcion };

			}
			else if (escenario.Resultado == "No exitoso")
			{

			 resultado = new Respuesta { CodEstado = (EstadoOperacion)escenario.CodEstado, CodMotivoRechazo = escenario.CodMotivoRechazo, Descripcion = escenario.Descripcion };

			}
			

			return resultado;

		}

	}
}
