﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SinpeMovil.Servicios.Modelos;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
	public class AyudanteSuscripcion : AyudanteBase<EscenarioSuscripcion>
	{
		public AyudanteSuscripcion()
		{
			_listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioSuscripcion>(Recursos.Recursos.Prueba_Suscripcion_Dummy);
		}

		public override List<EscenarioSuscripcion> ListaEscenarios
		{
			get { return _listaEscenarios; }
		}

		public Respuesta ObtenerRespuesta(Suscripcion suscripcion)
		{
            Respuesta resultado = null;
            EscenarioSuscripcion escenario = ListaEscenarios.FirstOrDefault(e => e.numTelefono == suscripcion.NumTelefono);

            if (escenario.Resultado == "Exitoso")
	        {
                resultado = new Respuesta { CodEstado = (EstadoOperacion)escenario.codEstado, Descripcion = escenario.Descripcion };
	        }
            else if (escenario.Resultado == "No exitoso")
            {
                resultado = new Respuesta { CodEstado = (EstadoOperacion)escenario.codEstado, CodMotivoRechazo = escenario.CodMotivoRechazo, Descripcion = escenario.Descripcion };
            }
            return resultado;
		}
	}
}
