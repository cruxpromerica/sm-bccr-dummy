﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
	public class AyudanteObtenerInformacionTelefono : AyudanteBase<EscenarioObtenerInformacionTelefono>
	{
		public override List<EscenarioObtenerInformacionTelefono> ListaEscenarios
		{
			get
			{
				return _listaEscenarios;
			}
		}

		public AyudanteObtenerInformacionTelefono()
		{
			_listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioObtenerInformacionTelefono>(Recursos.Recursos.PruebaObtenerInformacionTelefono);
		}

        public Suscripcion ObtenerInformacionTelefono(int numTelefono)
		{
			Suscripcion resultado = null;
			var escenario = ListaEscenarios.FirstOrDefault(e => e.NumTelefono == numTelefono);

			if (escenario != null)
			{

				if (escenario.LanzaExcepcion == "TRUE")
				{
					if (escenario.MensajeErrorRecibido == "Excepción (no tipificada en estándar electrónico)")
						throw new Exception();
					else
						throw new Exception(escenario.MensajeExcepcion);
				}
				else
				{
					resultado = new Suscripcion()
					{
						CodEntidad = escenario.CodEntidad,
						Identificacion = escenario.Identificacion,
						NombreCliente = escenario.NombreCliente,
						NumTelefono = escenario.NumTelefono
					};
				}
			}

			return resultado;
		}
	}
}