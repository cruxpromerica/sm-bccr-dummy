﻿using Comunes.Pruebas;
using SinpeMovil.Dummy.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SinpeMovil.Servicios.Modelos;

namespace SinpeMovil.Dummy.Utilitarios.Ayudantes
{
    public class AyudantePagoDesdeCuenta : AyudanteBase<EscenarioPagoDesdeCuenta>
    {
        public AyudantePagoDesdeCuenta()
		{
			_listaEscenarios = new ObtenerEscenarios().ObtenerListaEscenariosDesdeArchivo<EscenarioPagoDesdeCuenta>(Recursos.Recursos.EnviarPagoDesdeCuenta_Dummy);
		}

        public override List<EscenarioPagoDesdeCuenta> ListaEscenarios
        {
            get { return _listaEscenarios; }
        }

        public RespuestaPago EnviarPagoDesdeCuenta(Operacion operacion)
        {
            RespuestaPago resultado = null;
            EscenarioPagoDesdeCuenta escenario = ListaEscenarios.FirstOrDefault(e => e.CodReferencia == operacion.CodReferencia);

            if (escenario.CodEstado == EstadoOperacion.Autorizada)
            {
                resultado = new RespuestaPago { CodEstado = escenario.CodEstado, Descripcion = escenario.Descripcion, FecValor = escenario.FecValor, NombreClienteDestino = escenario.NombreClienteDestino };
            }
            else if (escenario.CodEstado == EstadoOperacion.Rechazada)
            {
                resultado = new RespuestaPago { CodEstado = escenario.CodEstado, CodMotivoRechazo = escenario.CodMotivoRechazo, Descripcion = escenario.Descripcion, FecValor = escenario.FecValor, NombreClienteDestino = escenario.NombreClienteDestino };
            }            
            return resultado;
        }
    }
}
