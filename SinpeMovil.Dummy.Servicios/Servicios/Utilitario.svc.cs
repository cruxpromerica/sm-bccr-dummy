﻿using SinpeMovil.Dummy.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas;
using System.Transactions;
using SinpeMovil.Dummy.AccesoDatos;
using Microsoft.Practices.Unity;
using SinpeMovil.LogicaNegocio.Modelos;

namespace SinpeMovil.Dummy.Servicios
{

	public class Utilitario : IUtilitario
	{

		public void BorrarPadronesDummy(int numTelefono)
		{
			using (var contexto = new ContextoDatos())
			{
				Dictionary<string, object> parametros = new Dictionary<string, object>();
				parametros.Add("NUM_TELEFONO", numTelefono);
				contexto.EjecutarSP("pr_sm_BorrarPadron_Dummy", parametros);
			}
		}

		public void BorrarTransaccionesDummy(string codReferencia)
		{
			using (var contexto = new ContextoDatos())
			{
				Dictionary<string, object> parametros = new Dictionary<string, object>();
				parametros.Add("COD_REFERENCIA", codReferencia);
				contexto.EjecutarSP("PR_SM_BORRAR_TRANSACCIONES_DUMMY", parametros);
			}
		}

		public void RegistrarTransacciones(Transaccion transaccion, TransaccionHistorica transaccionHST)
		{
			using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
			{
				IsolationLevel = IsolationLevel.ReadCommitted
			}))
			{
				using (var repositorioTransacciones = ContenedorUnity.Instancia.Resolve<Repositorios.Transacciones>())
				{
					var repositorioPadrones = ContenedorUnity.Instancia.Resolve<Repositorios.PadronesMoviles>();
					var idPadron = repositorioPadrones.ObtenerPadronPorNumTelefono(transaccion.NumTelefono).IdPadron;

					var transac = Mapper.MapObjects<Transaccion, Dummy.Modelos.Transaccion>(transaccion);

					if (transaccion.CodEntidadDestino == null)
						transac.CodEntidadDestino = 0;
					if (transaccion.CodEntidadOrigen == null)
						transac.CodEntidadOrigen = 0;

					transac.PadronId = idPadron;
					transac.CodEstado = transaccionHST.CodEstado;
					transac.Descripcion = transaccionHST.Descripcion;
					transac.FecActualizacion = transaccionHST.FecActualizacion;
					transac.CodRechazo = (int)transaccionHST.CodRechazo;
					transac.FecValor = transaccionHST.FecValor;
					
					repositorioTransacciones.Agregar<Dummy.Modelos.Transaccion>(transac);
				}

				scope.Complete();
			}   
		}
	}
}