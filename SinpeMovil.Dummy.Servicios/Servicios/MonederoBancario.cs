﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SinpeMovil.Servicios.Modelos;
using SinpeMovil.Dummy.Contratos;
using Microsoft.Practices.Unity;
using SinpeMovil.LogicaNegocio.Dominio.Caracteristicas;
using SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas;
using System.ServiceModel;

namespace SinpeMovil.Dummy.Servicios
{
	[ServiceBehavior(Namespace = "http://bccr.fi.cr/servicios/pmb")]
	public class MonederoBancario : IMonederoBancario
	{
		public bool ServicioDisponible()
		{
			return true;
		}

        public Respuesta SolicitarMonedero(Suscripcion suscripcion)
        {
			var caracteristica = new SuscribirPadronMovil();
			return caracteristica.SolicitarMonedero(suscripcion);
        }

        public Respuesta InactivarMonedero(Suscripcion suscripcion)
        {
			var caracteristica = ContenedorUnity.Instancia.Resolve<InactivarMonedero>();
			return caracteristica.InactivarMonederoMovil(suscripcion);
        }

        public Suscripcion ObtenerInformacionTelefono(int numTelefono)
        {
			var caracteristica = ContenedorUnity.Instancia.Resolve<InformacionTelefono>();
			return caracteristica.ObtenerInformacionTelefono(numTelefono);
        }

		public void PermitirRecibirTransacciones(bool permitir)
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<RecibirTransacciones>();
			caracteristica.PermitirRecibirTransacciones(permitir);
		}

		public RespuestaPago EnviarPagoDesdeTelefono(Operacion pago)
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<PagoDesdeTelefono>();
			return caracteristica.EnviarPagoDesdeTelefono(pago);
		}

		public RespuestaPago EnviarPagoDesdeCuenta(Operacion pago)
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<PagoDesdeCuenta>();
			return caracteristica.EnviarPagoDesdeCuenta(pago);
		}

		public RespuestaPago ObtenerEstado(string referencia)
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<ObtenerEstado>();
			return caracteristica.ObtenerEstadoTransaccion(referencia);
		}

		public List<DetalleOperacion> ObtenerDetalleOperaciones(DateTime fecLiquidacion)
		{
			var caracteristica = ContenedorUnity.Instancia.Resolve<DetalleOperaciones>();
			return caracteristica.ObtenerDetalleOperaciones(fecLiquidacion);
		}

	}
}