﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
    public class ObtenerEstado
    {
        public RespuestaPago ObtenerEstadoTransaccion(string codReferencia)
        {
            RespuestaPago respuestaPago = new RespuestaPago();
            using (var accionTransaccion = new Repositorios.Transacciones())
			{
				var transaccion = accionTransaccion.Obtener<Modelos.Transaccion>(p => p.CodReferencia == codReferencia);
                Action<Modelos.Transaccion, RespuestaPago> customAction = (Source, Destination) =>
                {
                    Destination.NombreClienteDestino = Source.NombreCliente;
                    Destination.CodMotivoRechazo = Source.CodRechazo;
					Destination.CodEstado = (EstadoOperacion)Source.CodEstado;
                };
                respuestaPago = Mapper.MapObjects<Modelos.Transaccion, RespuestaPago>(transaccion, customAction);
			}
            return respuestaPago;
        }
    }
}