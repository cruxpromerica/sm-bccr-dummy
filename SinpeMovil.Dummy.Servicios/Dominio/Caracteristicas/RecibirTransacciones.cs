﻿using SinpeMovil.Dummy.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
    public class RecibirTransacciones
    {
        public void PermitirRecibirTransacciones(bool permitir)
        {
            var PadronesMoviles = new PadronesMoviles();
            PadronesMoviles.PermitirRecibirTransacciones(permitir);
        }
    }
}