﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Text;
using Microsoft.Practices.Unity;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
	public class SuscribirPadronMovil
	{
		public Respuesta SolicitarMonedero(Suscripcion suscripcion)
		{
			SinpeMovil.Servicios.Modelos.Respuesta respuesta = new SinpeMovil.Servicios.Modelos.Respuesta();

			var cliente = Mapper.MapObjects<Suscripcion, Modelos.Cliente>(suscripcion);
			var padron = Mapper.MapObjects<Suscripcion, Modelos.Padron>(suscripcion);

			string telefono = suscripcion.NumTelefono.ToString();

			if (telefono.EndsWith("011") || telefono.EndsWith("012"))
			{
				respuesta.CodEstado = EstadoOperacion.Rechazada;
				respuesta.CodMotivoRechazo = 2;
				respuesta.Descripcion = "Número de identificación inválida";
				return respuesta;
			}

			if (telefono.EndsWith("013"))
			{
				respuesta.CodEstado = EstadoOperacion.Rechazada;
				respuesta.CodMotivoRechazo = 28;
				respuesta.Descripcion = "Cuenta asociada al número de teléfono no existe";
				return respuesta;
			}
			if (telefono.EndsWith("014"))
			{
				respuesta.CodEstado = EstadoOperacion.Rechazada;
				respuesta.CodMotivoRechazo = 18;
				respuesta.Descripcion = "El número de teléfono ya está suscrito para otro cliente";
				return respuesta;
			}

			/*if (telefono.EndsWith("010"))
			{
				respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Autorizada;
				respuesta.Descripcion = "Autorizada";
			}*/
			else
			{
				try
				{
					using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
					{
						IsolationLevel = IsolationLevel.ReadCommitted
					}))
					{
						using (var repositorioClientes = ContenedorUnity.Instancia.Resolve<Repositorios.Clientes>())
						{
							var clienteEncontrado = repositorioClientes.Obtener<SinpeMovil.Dummy.Modelos.Cliente>(x => x.Identificacion == suscripcion.Identificacion);

							if (clienteEncontrado == null)
							{
								cliente.CodEstado = (int)EstadosPadronMovil.Activo;
								repositorioClientes.Agregar<Modelos.Cliente>(cliente);
							}

							using (var repositorio = ContenedorUnity.Instancia.Resolve<Repositorios.PadronesMoviles>())
							{
								var padronEncontrado = repositorio.Obtener<SinpeMovil.Dummy.Modelos.Padron>(padronMovil => padronMovil.NumTelefono == suscripcion.NumTelefono, new string[] { "Cliente" });

								if (padronEncontrado != null)
								{
									if (padronEncontrado.CodEstado == (int)EstadosPadronMovil.Inactivo)
									{
										padronEncontrado.CodEstado = (int)EstadosPadronMovil.Activo;
										repositorio.Actualizar<SinpeMovil.Dummy.Modelos.Padron>(padronEncontrado);
										respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Autorizada;
										respuesta.Descripcion = "Autorizada";
									}
									else
									{
										respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Rechazada;
										respuesta.Descripcion = "Rechazada";
									}
								}
								else
								{
									clienteEncontrado = repositorioClientes.Obtener<SinpeMovil.Dummy.Modelos.Cliente>(x => x.Identificacion == suscripcion.Identificacion);

									padron.ClienteId = clienteEncontrado.IdCliente;
									padron.CodEstado = (int)EstadosPadronMovil.Activo;

									repositorio.Agregar<SinpeMovil.Dummy.Modelos.Padron>(padron);

									respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Autorizada;
									respuesta.Descripcion = "Autorizada";
								}
							}
						}

						scope.Complete();
					}
				}
				catch (Exception ex)
				{
					respuesta = new SinpeMovil.Servicios.Modelos.Respuesta()
					{
						CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Rechazada,
						Descripcion = ex.Message
					};
				}
			}

			return respuesta;
			
		}
	}
}