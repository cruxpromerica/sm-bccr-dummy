﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using Microsoft.Practices.Unity;
using System.ServiceModel;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
	public class DetalleOperaciones
	{
		public List<DetalleOperacion> ObtenerDetalleOperaciones(DateTime fecLiquidacion)
		{
			List<DetalleOperacion> listaDetalles = new List<DetalleOperacion>();
			string fecha = fecLiquidacion.ToString("dd/MM/yyyy");

			if (fecha == "30/01/2014")
			{
				return new List<DetalleOperacion>();
			}
			else if (fecha == "31/01/2014") 
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException(string.Format("El ciclo consultado para la fecha de liquidación [{0}] aún no ha cerrado", fecha), 25);
			}
			else if (fecha == "30/03/2014")
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException(string.Format("La fecha de liquidación [{0}] dada es incorrecta", fecha), 17);
			}
			else if (fecha == "29/01/2014")
			{
				DetalleOperacion detalle1 = new DetalleOperacion
				{
					CodEntidadDestino = 100,
					CodEntidadOrigen = 112,
					CodMoneda = 1,
					CodReferencia = "1098234568008230982345671",
					FecValor = new DateTime(2014, 01, 28),
					Monto = 100000
				};

				listaDetalles.Add(detalle1);

				DetalleOperacion detalle2 = new DetalleOperacion
				{
					CodEntidadDestino = 100,
					CodEntidadOrigen = 112,
					CodMoneda = 2,
					CodReferencia = "2993091234561020912387456",
					FecValor = new DateTime(2014, 01, 28),
					Monto = 1000
				};

				listaDetalles.Add(detalle2);

				return listaDetalles;
			}
			else
			{
				fecLiquidacion = fecLiquidacion.AddDays(-1);

				using (var repositorioTransacciones = ContenedorUnity.Instancia.Resolve<Repositorios.Transacciones>())
				{
					var listaTransacciones = repositorioTransacciones.ListarTransacciones(fecLiquidacion);

					foreach (var item in listaTransacciones)
					{
						DetalleOperacion detalle = new DetalleOperacion
						{
							CodEntidadDestino = (int)item.CodEntidadDestino,
							CodEntidadOrigen = (int)item.CodEntidadOrigen,
							CodMoneda = (short)item.CodMoneda,
							CodReferencia = item.CodReferencia,
							FecValor = item.FecValor,
							Monto = item.Monto
						};

						listaDetalles.Add(detalle);
					}
				}

				return listaDetalles;
			}

		}
	}
}