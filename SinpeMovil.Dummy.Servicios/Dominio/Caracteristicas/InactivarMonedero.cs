﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using Microsoft.Practices.Unity;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
    public class InactivarMonedero
    {
        public Respuesta InactivarMonederoMovil(Suscripcion suscripcion)
        {

			SinpeMovil.Servicios.Modelos.Respuesta respuesta = new SinpeMovil.Servicios.Modelos.Respuesta();

			var cliente = Mapper.MapObjects<Suscripcion, Modelos.Cliente>(suscripcion);
			var padron = Mapper.MapObjects<Suscripcion, Modelos.Padron>(suscripcion);

			string telefono = suscripcion.NumTelefono.ToString();

			if (telefono.EndsWith("016"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("El número de teléfono utilizado no está suscrito al servicio SINPE Móvil", 20);
			}

			if (telefono.EndsWith("017"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("El número de teléfono destino no está suscrito al servicio o está inactivo", 11);
			}

			if (telefono.EndsWith("015"))
			{
				respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Autorizada;;
				respuesta.Descripcion = "Autorizada";
				return respuesta;
			}
            else if (!telefono.EndsWith("016") || !telefono.EndsWith("017"))
			{
				try
				{
					using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
					{
						IsolationLevel = IsolationLevel.ReadCommitted
					}))
					{
						using (var repositorio = ContenedorUnity.Instancia.Resolve<Repositorios.PadronesMoviles>())
						{

							var padronEncontrado = repositorio.Obtener<SinpeMovil.Dummy.Modelos.Padron>(padronMovil => padronMovil.NumTelefono == suscripcion.NumTelefono && padronMovil.CodEstado == 2, new string[] { "Cliente" });
							
							if (padronEncontrado != null)
							{
								padronEncontrado.CodEstado = (int)EstadosPadronMovil.Inactivo;
								repositorio.Actualizar<SinpeMovil.Dummy.Modelos.Padron>(padronEncontrado);

								respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Autorizada;
								respuesta.Descripcion = "Autorizada";
							}
							else
							{
								respuesta.CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Rechazada;
								respuesta.Descripcion = "Rechazada";
							}	
						}

						scope.Complete();
					}

				}
				catch (Exception ex)
				{
					respuesta = new SinpeMovil.Servicios.Modelos.Respuesta()
					{
						CodEstado = SinpeMovil.Servicios.Modelos.EstadoOperacion.Rechazada,
						Descripcion = ex.Message
					};
				}
			}

			return respuesta;
		}
	}
}