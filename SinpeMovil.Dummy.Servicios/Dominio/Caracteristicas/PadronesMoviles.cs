﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Practices.Unity;
using SinpeMovil.Servicios.Modelos;
using SinpeMovil.Dummy.AccesoDatos;

namespace SinpeMovil.LogicaNegocio.Dominio.Caracteristicas
{
	public class PadronesMoviles 
	{
        public Respuesta SolicitarMonedero(Suscripcion suscripcion)
		{
            var PadronesMoviles = ContenedorUnity.Instancia.Resolve<Dummy.AccesoDatos.PadronesMoviles>();
            return PadronesMoviles.SolicitarMonedero(suscripcion);
		}
	}
}
