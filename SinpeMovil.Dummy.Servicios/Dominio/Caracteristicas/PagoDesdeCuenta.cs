﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using SinpeMovil.Dummy.Modelos;
using System.Transactions;
using System.Threading;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
    public class PagoDesdeCuenta
    {
        public RespuestaPago EnviarPagoDesdeCuenta(Operacion pago)
        {
            //var PadronesMoviles = new PadronesMoviles();
            //return PadronesMoviles.EnviarPagoDesdeCuenta(pago);

			SinpeMovil.Servicios.Modelos.RespuestaPago respuestaPago = new SinpeMovil.Servicios.Modelos.RespuestaPago();

			string telefonoDestino = pago.NumTelefonoDestino.ToString();
			string telefonoOrigen = pago.NumTelefonoOrigen.ToString();

			if (telefonoDestino.EndsWith("031"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Autorizada;
				respuestaPago.Descripcion = "Autorizada";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}

			if (telefonoDestino.EndsWith("032"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 23;
				respuestaPago.Descripcion = "Cuenta asociada al número de teléfono cerrada";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("033"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 31;
				respuestaPago.Descripcion = "Cuenta asociada al número de teléfono bloqueada";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("034"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 28;
				respuestaPago.Descripcion = "Cuenta asociada al número de teléfono no existe";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("035"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 4;
				respuestaPago.Descripcion = "El número de teléfono no se encuentra registrado";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("036"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 91;
				respuestaPago.Descripcion = "Moneda no corresponde";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("037"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 83;
				respuestaPago.Descripcion = "Problemas en la respuesta del destino";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}
			if (telefonoDestino.EndsWith("038") || telefonoDestino.EndsWith("039"))
			{
                throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("El número de teléfono destino no está suscrito al servicio o está inactivo", 11);
			}
			if (telefonoDestino.EndsWith("040"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("La entidad destino está inactiva o no pertenece al servicio SINPE Móvil", 22);
			}
			if (telefonoDestino.EndsWith("041"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("La entidad no está disponible para recibir transacciones", 24);
			}

			if (telefonoDestino.EndsWith("048"))
			{
				Thread.Sleep(10000);
				respuestaPago.CodEstado = EstadoOperacion.Autorizada;
				respuestaPago.Descripcion = "Autorizada";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}

			if (telefonoDestino.EndsWith("050"))
			{
				Thread.Sleep(10000);
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 76;
				respuestaPago.Descripcion = "Problemas de comunicación";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}

			if (telefonoDestino.EndsWith("052"))
			{
				respuestaPago.CodEstado = EstadoOperacion.Rechazada;
				respuestaPago.CodMotivoRechazo = 0;
				respuestaPago.Descripcion = "Ocurrió un error en el servicio SINPE Móvil";
				respuestaPago.FecValor = DateTime.Now;
				respuestaPago.NombreClienteDestino = pago.Descripcion;
				return respuestaPago;
			}

			var accionTransaccion = ContenedorUnity.Instancia.Resolve<Caracteristicas.Transacciones>();
			var accionPadronMovil = ContenedorUnity.Instancia.Resolve<Repositorios.PadronesMoviles>();

			Transaccion transaccion = new Transaccion();

			Action<Operacion, Transaccion> customAction = (Source, Destination) =>
			{
				Destination.CodMoneda = (int)Source.Moneda;
			};

			transaccion = Mapper.MapObjects<Operacion, Transaccion>(pago, customAction);
			transaccion.TipoTransaccion = 1; //Origen=1, Destino=2
			transaccion.PadronId = accionPadronMovil.Obtener<SinpeMovil.Dummy.Modelos.Padron>(padronMovil => padronMovil.NumTelefono == pago.NumTelefonoOrigen, new string[] { "Cliente" }).IdPadron; ;
			transaccion.FecRegistro = DateTime.Now;

			transaccion.CodEstado = (int)EstadoOperacion.Autorizada;
			transaccion.CodRechazo = 0;
			transaccion.Descripcion = "Registro de Transaccion";
			transaccion.FecActualizacion = DateTime.Now;
	
			using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }))
			{
				accionTransaccion.RegistrarTransaccion(transaccion);
				scope.Complete();
			}

			respuestaPago.CodEstado = EstadoOperacion.Autorizada;
			respuestaPago.Descripcion = "Autorizada";
			respuestaPago.FecValor = DateTime.Now;
			respuestaPago.NombreClienteDestino = pago.Descripcion;

			return respuestaPago;

        }
    }
}