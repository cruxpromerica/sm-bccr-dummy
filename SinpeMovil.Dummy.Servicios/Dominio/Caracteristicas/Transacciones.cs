﻿using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Text;
using Microsoft.Practices.Unity;
using SinpeMovil.Dummy.Modelos;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
    public class Transacciones 
    {
        public void RegistrarTransaccion(Transaccion transaccion)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            }))
            {
				using (var repositorioTransacciones = ContenedorUnity.Instancia.Resolve<Repositorios.Transacciones>())
				{
					repositorioTransacciones.Registrar(transaccion);
				}
    
                scope.Complete();
            }   

        }

        /*public bool ActualizarTransaccion(SinpeMovil.LogicaNegocio.Modelos.TransaccionHistorica transaccionHistorica)
        {
            bool res = false;
            var accionTransaccion = ContenedorUnity.Instancia.Resolve<Acciones.Transaccion>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            }))
            {
                accionTransaccion.ActualizarTransaccion(transaccionHistorica);
                res = true;
                scope.Complete();
            }

            return res;
        }*/

       /* public SinpeMovil.LogicaNegocio.Modelos.TransaccionHistorica ObtenerEstado(string codReferencia)
        {
            SinpeMovil.LogicaNegocio.Modelos.TransaccionHistorica transaccionHistorica = new SinpeMovil.LogicaNegocio.Modelos.TransaccionHistorica();
            var accionTransaccion = ContenedorUnity.Instancia.Resolve<Acciones.Transaccion>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            }))
            {
                transaccionHistorica = accionTransaccion.ObtenerEstado(codReferencia);
                scope.Complete();
            }

            return transaccionHistorica;
        }*/
    }
}
