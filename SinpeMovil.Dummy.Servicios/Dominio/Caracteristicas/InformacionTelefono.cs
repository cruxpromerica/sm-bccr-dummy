﻿
using SinpeMovil.Dummy.AccesoDatos;
using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SinpeMovil.Dummy.Servicios.Dominio.Caracteristicas
{
	public class InformacionTelefono
	{
		public Suscripcion ObtenerInformacionTelefono(int numTelefono)
		{
			Suscripcion respuesta = null;
			SinpeMovil.Dummy.Modelos.Padron padron = null;

			string telefono = numTelefono.ToString();

			if (telefono.EndsWith("001"))
			{
				throw new Exception("Excepción (no tipificada en estándar electrónico)");
			}
			if (telefono.EndsWith("002"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("El número de teléfono utilizado no está suscrito al servicio SINPE Móvil", 20);
			}
			if (telefono.EndsWith("003"))
			{
				throw new SinpeMovil.LogicaNegocio.Modelos.SinpeMovilException("El número de teléfono destino no está suscrito al servicio o está inactivo", 11);
			}
			/*if (telefono.EndsWith("004"))
			{
				respuesta.NumTelefono = 88888021;
				respuesta.Identificacion = "01-0111-0111";
				respuesta.NombreCliente = "Bobby Fischer";
				respuesta.CodEntidad = 152;
				return respuesta;
			}*/

			using (var repositorio = new Repositorios.PadronesMoviles())
			{
				padron= repositorio.Obtener<Modelos.Padron>(p => p.NumTelefono == numTelefono, new string[] {"Cliente"});
			}

			if (padron != null)
			{
				if (padron.CodEstado == (int)EstadosPadronMovil.Activo)
				{
					Action<SinpeMovil.Dummy.Modelos.Padron, SinpeMovil.Servicios.Modelos.Suscripcion> customAction = (Source, Destination) =>
					{
						Destination.Identificacion = Source.Cliente.Identificacion;
						Destination.NombreCliente = Source.Cliente.NombreCliente;

					};

					respuesta = Mapper.MapObjects<Modelos.Padron, Suscripcion>(padron, customAction);
				}
				
			}

			return respuesta;
		}
	}
}