﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SinpeMovil.Dummy.Modelos
{
    [KnownType(typeof(Cliente))]
    [Serializable]
    public class Padron : ModeloBase
    {
        [Key]
        [DataMember]
        public int IdPadron { get; set; }

        [DataMember]
        public int ClienteId { get; set; }

        [DataMember]
        public int NumTelefono { get; set; }

        [DataMember]
        public int CodEntidad { get; set; }

        [DataMember]
        public int CodEstado { get; set; }

        public Cliente Cliente { get; set; }
    }
}
