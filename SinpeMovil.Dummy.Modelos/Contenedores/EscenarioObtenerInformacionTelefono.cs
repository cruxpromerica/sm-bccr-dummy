﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Modelos
{
	public class EscenarioObtenerInformacionTelefono : EscenarioBase
	{
		int mNumTelefono;
		string ID;
		string mNombreCliente;
		int mCodEntidad;
		string mEstadoPadron;
		int mMoneda;
		int mTipoID;
		long mCuentaCliente;

		public int NumTelefono
		{
			get { return mNumTelefono; }
			set { mNumTelefono = value; }
		}

		public string Identificacion
		{
			get { return ID; }
			set { ID = value; }
		}
		public string NombreCliente
		{
			get { return mNombreCliente; }
			set { mNombreCliente = value; }
		}
		public int CodEntidad
		{
			get { return mCodEntidad; }
			set { mCodEntidad = value; }
		}
		public string EstadoPadron
		{
			get { return mEstadoPadron; }
			set { mEstadoPadron = value; }
		}

		public int TipoID
		{
			get { return mTipoID; }
			set { mTipoID = value; }
		}

		public int Moneda
		{
			get { return mMoneda; }
			set { mMoneda = value; }
		}
		public long CuentaCliente
		{
			get { return mCuentaCliente; }
			set { mCuentaCliente = value; }
		}

		public string MensajeErrorRecibido { get; set; }

		public string Resultado { get; set; }

		public override string ToString()
		{
			return String.Format("NumTelefono: {0}, Identificacion: {1}, NombreCliente: {2}", NumTelefono, Identificacion, NombreCliente);
		}

		public string LanzaExcepcion { get; set; }

		public string MensajeExcepcion { get; set; }
	}
}
