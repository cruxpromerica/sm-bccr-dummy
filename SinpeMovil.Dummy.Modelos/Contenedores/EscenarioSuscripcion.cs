﻿using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SinpeMovil.Dummy.Modelos
{
	public class EscenarioSuscripcion : EscenarioBase
	{
        [DataMember]
        public int CodEntidad { get; set; }

        [DataMember]
        public int codEstado { get; set; }

        [DataMember]
        public int numTelefono { get; set; }

        [DataMember]
        public string nombreCliente { get; set; }

        [DataMember]
        public string Identificacion { get; set; }

        [DataMember]
        public int? CodMotivoRechazo { get; set; }

        [DataMember]
        public string Resultado { get; set; }

        [DataMember]
        public string Descripcion { get; set; }
	}
}
