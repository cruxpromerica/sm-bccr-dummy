﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Modelos
{
	public class EscenarioObtenerDetalleOperaciones : EscenarioBase
	{

		DateTime mFecLiquidacion;

		[DataMember]
		public DateTime FecLiquidacion
		{
			get { return mFecLiquidacion; }
			set { mFecLiquidacion = value; }
		}

        string mCodReferencia;

		[DataMember ]
		public string CodReferencia
		{
			get { return mCodReferencia; }
			set { mCodReferencia = value; }
		}


		int mCodEntidadOrigen;

		[DataMember]
		public int CodEntidadOrigen
		{
			get { return mCodEntidadOrigen; }
			set { mCodEntidadOrigen = value; }
		}


		int mCodEntidadDestino;

		[DataMember]
		public int CodEntidadDestino
		{
			get { return mCodEntidadDestino; }
			set { mCodEntidadDestino = value; }
		}


		int mMonto;

		[DataMember]
		public int Monto
		{
			get { return mMonto; }
			set { mMonto = value; }
		}


        short mCodMoneda;

		[DataMember]
        public short CodMoneda
		{
			get { return mCodMoneda; }
			set { mCodMoneda = value; }
		}


		DateTime mFecValor;

		[DataMember]
		public DateTime FecValor
		{
			get { return mFecValor; }
			set { mFecValor = value; }
		}

		string mResultado;

		[DataMember]
		public string Resultado
		{
			get { return mResultado; }
			set { mResultado = value; }
		}

		string mMensaje;

		[DataMember]
		public string Mensaje
		{
			get { return mMensaje; }
			set { mMensaje = value; }
		}

	}
}
