﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Modelos
{
	public class EscenarioInactivar : EscenarioBase 
	{

		int mNumTelefono;

		[DataMember]
		public int NumTelefono
		{
			get { return mNumTelefono; }
			set { mNumTelefono = value; }
		}

		string ID;

		[DataMember]
		public string Identificador
		{
			get { return ID; }
			set { ID = value; }
		}

		string mNombreCliente;

		[DataMember]
		public string NombreCliente
		{
			get { return mNombreCliente; }
			set { mNombreCliente = value; }
		}

		int mCodEntidad;

		[DataMember]
		public int CodEntidad
		{
			get { return mCodEntidad; }
			set { mCodEntidad = value; }
		}

		int mCodEstado;

		[DataMember]
		public int CodEstado
		{
			get { return mCodEstado; }
			set { mCodEstado = value; }
		}

		int mCodMotivoRechazo;

		[DataMember]
		public int CodMotivoRechazo
		{
			get { return mCodMotivoRechazo; }
			set { mCodMotivoRechazo = value; }
		}

		string mDescripcion;

		[DataMember]
		public string Descripcion
		{
			get { return mDescripcion; }
			set { mDescripcion = value; }
		}

		[DataMember]
		string mResultado;

		public string Resultado
		{
			get { return mResultado; }
			set { mResultado = value; }
		}

	}
}
