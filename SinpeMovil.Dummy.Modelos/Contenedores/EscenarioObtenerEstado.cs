﻿using SinpeMovil.Servicios.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Modelos.Contenedores
{
    public class EscenarioObtenerEstado : EscenarioBase
    {
        [DataMember]
        public EstadoOperacion CodEstado { get; set; }

        [DataMember]
        public int? CodMotivoRechazo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        [DataMember]
        public DateTime FecValor { get; set; }

        [DataMember]
        public string NombreClienteDestino { get; set; }

        [DataMember]
        public string CodReferencia { get; set; }
    }
}
