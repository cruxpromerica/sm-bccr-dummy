﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using SinpeMovil.Dummy.Modelos;


namespace SinpeMovil.Dummy.Modelos
{
    [Serializable()]
    [KnownType(typeof(Transaccion))]
	public class Transaccion : ModeloBase
    {

		int mIdTransaccion;
		[Key()]
		[DataMember()]
		public int IdTransaccion
		{
			get { return mIdTransaccion; }
			set { mIdTransaccion = value; }
		}

		private string mCodReferencia;
		[StringLength(Constantes.TamanoMaximoNumeroCodRef)]
		[Required()]
		[DataMember()]
		public string CodReferencia
		{
			get { return mCodReferencia; }
			set { mCodReferencia = value; }
		}

		private int mPadronId;
		[Required()]
		[DataMember()]
		public int PadronId
		{
			get { return mPadronId; }
			set { mPadronId = value; }
		}

		private int? mCodEntidadOrigen;
		[DataMember()]
		public int? CodEntidadOrigen
		{
			get { return mCodEntidadOrigen; }
			set { mCodEntidadOrigen = value; }
		}

		private string mIdCliente;
		[Required()]
		[DataMember()]
		public string IdCliente
		{
			get { return mIdCliente; }
			set { mIdCliente = value; }
		}

		private string mNombreCliente;
		[StringLength(Constantes.TamanoMaximoNombreCliente)]
		[Required()]
		[DataMember()]
		public string NombreCliente
		{
			get { return mNombreCliente; }
			set { mNombreCliente = value; }
		}

		private int mNumTelefono;
		[Required()]
		[DataMember()]
		public int NumTelefono
		{
			get { return mNumTelefono; }
			set { mNumTelefono = value; }
		}

		private int? mCodEntidadDestino;
		[DataMember()]
		public int? CodEntidadDestino
		{
			get { return mCodEntidadDestino; }
			set { mCodEntidadDestino = value; }
		}

		private int mCodMoneda;
		[Required()]
		[DataMember()]
		public int CodMoneda
		{
			get { return mCodMoneda; }
			set { mCodMoneda = value; }
		}

		private int mMonto;
		[Required()]
		[DataMember()]
		public int Monto
		{
			get { return mMonto; }
			set { mMonto = value; }
		}

		private string mDesTransaccion;
		[StringLength(Constantes.TamanoMaximoDescTransaccion)]
		[Required()]
		[DataMember()]
		public string DesTransaccion
		{
			get { return mDesTransaccion; }
			set { mDesTransaccion = value; }
		}

		private int mTipoTransaccion;
		[Required()]
		[DataMember()]
		public int TipoTransaccion
		{
			get { return mTipoTransaccion; }
			set { mTipoTransaccion = value; }
		}

		private DateTime mFecRegistro;
		[Required()]
		[DataMember()]
		public DateTime FecRegistro
		{
			get { return mFecRegistro; }
			set { mFecRegistro = value; }
		}
		/// <summary>
		/// /////////////////////////////////////////////////////////////////
		/// </summary>
		private int mCodEstado;
		[Required()]
		[DataMember()]
		public int CodEstado
		{
			get { return mCodEstado; }
			set { mCodEstado = value; }
		}

		private string mDescripcion;
		[StringLength(Constantes.TamanoMaximoDescTransaccionHist)]
		[Required()]
		[DataMember()]
		public string Descripcion
		{
			get { return mDescripcion; }
			set { mDescripcion = value; }
		}

		private DateTime mFechaActualizacion;
		[Required()]
		[DataMember()]
		public DateTime FecActualizacion
		{
			get { return mFechaActualizacion; }
			set { mFechaActualizacion = value; }
		}

		private int mCodRechazo;
		[Required()]
		[DataMember()]
		public int CodRechazo
		{
			get { return mCodRechazo; }
			set { mCodRechazo = value; }
		}

		private DateTime mFecValor;
		[Required()]
		[DataMember()]
		public DateTime FecValor
		{
			get { return mFecValor; }
			set { mFecValor = value; }
		}

    }
}
