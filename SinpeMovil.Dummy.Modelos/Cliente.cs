﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SinpeMovil.Dummy.Modelos
{
	[KnownType(typeof(Cliente))]
	[Serializable()]
	public class Cliente : ModeloBase
	{
		private int mIdCliente;
		[Key]
		public int IdCliente
		{
			get { return mIdCliente; }
			set { mIdCliente = value; }
		}

		private int mCodTipoId;
		[Required]
		public int TipoId
		{
			get { return mCodTipoId; }
			set { mCodTipoId = value; }
		}

		private string mIdentificacion;
		[StringLength(12)]
		[Required()]
		public string Identificacion
		{
			get { return mIdentificacion; }
			set { mIdentificacion = value; }
		}

		private string mNombre;
        [StringLength(40)]
		[Required]
		public string NombreCliente
		{
			get { return mNombre; }
			set { mNombre = value; }
		}

		private int mCodEstado;
		[Required]
		public int CodEstado
		{
			get { return mCodEstado; }
			set { mCodEstado = value; }
		}

	}
}