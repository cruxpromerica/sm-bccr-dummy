﻿

namespace SinpeMovil.Dummy.Modelos
{
    public static class Constantes
    {
        // Estas variables son para validaciones 

        public const short CodServicioSinpeMovil = 83;
        //-----------------------------------------------

		// CLIENTE 
		public const int TamanoMaximoIdentificacionCliente = 12;
		public const int TamanoMaximoNombreCliente = 40;
		public const int TamanoMaximoCodInterno = 30;

		// CUENTA 
        public const int TamanoCuentaCliente = 17;

		// ENTIDAD
        public const int TamanoMaximoNombreEntidad = 100;

		// MONEDA 
        public const int TamanoMaximoNomMonedaIso = 50;
        public const int TamanoMaximoNombreMoneda = 50;

		// PARAMETRO 
        public const int TamanoMaximoNombreParametro = 100;

		// TRANSACCION  
        public const int TamanoMaximoNumeroCodRef = 25;
        public const int TamanoMaximoDescTransaccion = 20;

		// TRANSACCION HISTORICA
		public const int TamanoMaximoDescTransaccionHist = 255;

		// RESPUESTA
		public const int TamañoMaximoDescripcionRespuesta = 20;

    }
}